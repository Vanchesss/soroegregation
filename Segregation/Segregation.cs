﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segregation
{
    public class Segregation
    {
        /// <summary>
        /// Кубическая кристаллическая решётка
        /// </summary>
        public Cell[, ,] Structure;
        /// <summary>
        /// Потенциал Терсоффа
        /// </summary>
        private PotentialTersoff potentialTersoff = new PotentialTersoff();
        /// <summary>
        /// Потенциал Стиллинджера-Вебера
        /// </summary>
        private PotentialStillinger potentialStillinger = new PotentialStillinger();

        /// <summary>
        /// Создание кубической структуры
        /// </summary>
        /// <param name="length">Размер структуры</param>
        /// <param name="gePart">Доля германия в сплаве</param>
        public Segregation(int length, double gePart, double latPar)
        {
            Structure = new Cell[length, length, length];
            // Изменение параметра решётки
            if (latPar == 0)
                Atom.LatPar = 0.54307 * (1.0 - gePart) + 0.5660 * gePart;
            else Atom.LatPar = latPar;

            for (int x = 0; x < length; x++)
                for (int y = 0; y < length; y++)
                    for (int z = 0; z < length; z++)
                    {
                        Structure[x, y, z] = new Cell(x, y, z, (gePart >= 0.6 ? AtomType.Ge : AtomType.Si));
                    }


            // |Распредление Ge или Si в кристалле|
            // Общее число атомов в структуре
            double fullNumberOfAtoms = length * length * length * Cell.NumberOfAtoms;
            // Количество атомов выбранного типа (в зависимости от доли Германия)
            int numberOfAtoms = 0;

            if (gePart >= 0.6) numberOfAtoms = (int)Math.Round((1.0 - gePart) * fullNumberOfAtoms);
            else numberOfAtoms = (int)Math.Round(gePart * fullNumberOfAtoms);

            AtomType atomType = (gePart >= 0.6 ? AtomType.Si : AtomType.Ge);

            Random rand = new Random(DateTime.Now.Millisecond);
            SetRandomAtomType(numberOfAtoms, atomType, rand);
        }
        /// <summary>
        /// Случайное распределение
        /// </summary>
        /// <param name="numberOfAtoms">Количество распределяемых атомов</param>
        /// <param name="type">Тип распределяемых атомов</param>
        /// <param name="rand">Генератор случайных чисел</param>
        private void SetRandomAtomType(int numberOfAtoms, AtomType type, Random rand)
        {
            int length = Structure.GetLength(0);

            for (int count = 0; count < numberOfAtoms; )
            {
                int x = rand.Next(0, length);
                int y = rand.Next(0, length);
                int z = rand.Next(0, length);

                int temp = rand.Next(0, Cell.NumberOfAtoms);

                if (Structure[x, y, z].Atoms[temp].Type != type)
                {
                    Structure[x, y, z].Atoms[temp].Type = type;
                    count++;
                }
            }
        }

        /// <summary>
        /// Поиск первых соседей выбранного атома
        /// </summary>
        /// <param name="selAtom">Атом, для которого ищутся соседи</param>
        /// <param name="indexOfSelAtom">Индекс выбранного атома в ячейке</param>
        /// <returns></returns>
        private Atom[] SearchNeighbours(Atom selAtom, int indexOfSelAtom)
        {
            List<Atom> neighbours = new List<Atom>();
            // Радиус поиска соседей
            double radiusOfNeighbours = new Vector(Atom.LatPar * 0.25, Atom.LatPar * 0.25, Atom.LatPar * 0.25).Magnitude(Vector.Zero);
            radiusOfNeighbours += 1e-6;

            int length = Structure.GetLength(0);
            int x = selAtom.PositionCell.GetIntX;
            int y = selAtom.PositionCell.GetIntY;
            int z = selAtom.PositionCell.GetIntZ;

            for (int lx = -1; lx <= 1; lx++)
                for (int ly = -1; ly <= 1; ly++)
                    for (int lz = -1; lz <= 1; lz++)// Перебор соседей, где lx, ly, lz - шаги
                    {
                        Vector posCellNeighbour = new Vector(x + lx, y + ly, z + lz);// Позиция ячейки-соседа
                        Vector additive = Vector.Zero;// Добавка для периодических граничных условий

                        /*
                         * Если выбранная ячейка (x,y,z) на границе, а ячейка-сосед (posCellNeighbour) за границей,
                         * то берём ячейку в конце структуры и с помощью добавки (additive) переносим в нужную позицию
                         */

                        if (posCellNeighbour.x < 0)
                        {
                            posCellNeighbour.x = length - 1;
                            additive.x = -length;
                        }
                        if (posCellNeighbour.x >= length)
                        {
                            posCellNeighbour.x = 0;
                            additive.x = length;
                        }

                        if (posCellNeighbour.y < 0)
                        {
                            posCellNeighbour.y = length - 1;
                            additive.y = -length;
                        }
                        if (posCellNeighbour.y >= length)
                        {
                            posCellNeighbour.y = 0;
                            additive.y = length;
                        }

                        if (posCellNeighbour.z < 0)
                        {
                            posCellNeighbour.z = length - 1;
                            additive.z = -length;
                        }
                        if (posCellNeighbour.z >= length)
                        {
                            posCellNeighbour.z = 0;
                            additive.z = length;
                        }

                        // Перебор атомов в ячейке-соседе
                        for (int indexOfAtom = 0; indexOfAtom < Cell.NumberOfAtoms; indexOfAtom++)
                        {
                            // Создаем копию атома-соседа
                            Atom neighbour = Structure[posCellNeighbour.GetIntX, posCellNeighbour.GetIntY, posCellNeighbour.GetIntZ].Atoms[indexOfAtom];
                            // Размещаем копию
                            neighbour.PositionCell += additive;

                            // Чтобы не брать уже выбранный атом
                            if (neighbour.PositionCell.x == x && neighbour.PositionCell.y == y && neighbour.PositionCell.z == z
                                && indexOfAtom == indexOfSelAtom) continue;

                            double radius = Vector.Magnitude(neighbour.GlobalCoord, selAtom.GlobalCoord);

                            if (radius <= radiusOfNeighbours)
                            {
                                // Первый сосед для выбранного атома
                                neighbours.Add(neighbour);
                                // Ищем соседей первого соседа
                                SearchSecondNeighbours(neighbour, indexOfAtom, selAtom, indexOfSelAtom, ref neighbours);
                            }
                        }
                    }

            return neighbours.ToArray();
        }

        /// <summary>
        /// Поиск вторых соседей
        /// </summary>
        /// <param name="selAtom">Выбранный первый атом-сосед для которого ищутся соседи</param>
        /// <param name="indexOfSelAtom">Индекс выбранного атома в ячейке</param>
        /// <param name="startAtom">Исходный атом для которого ищутся первые и вторые соседи</param>
        /// <param name="indexOfStartAtom">Индекс исходного атома в ячейке</param>
        /// <param name="neighbours">Массив имеющихся соседей</param>
        private void SearchSecondNeighbours(Atom selAtom, int indexOfSelAtom, Atom startAtom, int indexOfStartAtom, ref List<Atom> neighbours)
        {
            // Радиус поиска соседей
            double radiusOfNeighbours = new Vector(Atom.LatPar * 0.25, Atom.LatPar * 0.25, Atom.LatPar * 0.25).Magnitude(Vector.Zero);
            radiusOfNeighbours += 1e-6;

            int length = Structure.GetLength(0);
            int x = selAtom.PositionCell.GetIntX;
            int y = selAtom.PositionCell.GetIntY;
            int z = selAtom.PositionCell.GetIntZ;

            for (int lx = -1; lx <= 1; lx++)
                for (int ly = -1; ly <= 1; ly++)
                    for (int lz = -1; lz <= 1; lz++)// Перебор соседей, где lx, ly, lz - шаги
                    {
                        Vector posCellNeighbour = new Vector(x + lx, y + ly, z + lz);// Позиция ячейки-соседа
                        Vector additive = Vector.Zero;// Добавка для периодических граничных условий

                        /*
                         * Если выбранная ячейка (x,y,z) на границе, а ячейка-сосед (posCellNeighbour) за границей,
                         * то берём ячейку в конце структуры и с помощью добавки (additive) переносим в нужную позицию
                         */

                        if (posCellNeighbour.x < 0)
                        {
                            posCellNeighbour.x += length;
                            additive.x = -length;
                        }
                        if (posCellNeighbour.x >= length)
                        {
                            posCellNeighbour.x -= length;
                            additive.x = length;
                        }

                        if (posCellNeighbour.y < 0)
                        {
                            posCellNeighbour.y += length;
                            additive.y = -length;
                        }
                        if (posCellNeighbour.y >= length)
                        {
                            posCellNeighbour.y -= length;
                            additive.y = length;
                        }

                        if (posCellNeighbour.z < 0)
                        {
                            posCellNeighbour.z += length;
                            additive.z = -length;
                        }
                        if (posCellNeighbour.z >= length)
                        {
                            posCellNeighbour.z -= length;
                            additive.z = length;
                        }

                        // Перебор атомов в ячейке-соседе
                        for (int indexOfAtom = 0; indexOfAtom < Cell.NumberOfAtoms; indexOfAtom++)
                        {
                            // Создаем копию атома-соседа
                            Atom neighbour = Structure[posCellNeighbour.GetIntX, posCellNeighbour.GetIntY, posCellNeighbour.GetIntZ].Atoms[indexOfAtom];
                            // Размещаем копию
                            neighbour.PositionCell += additive;

                            // Чтобы не брать уже выбранный атом
                            if (neighbour.PositionCell.x == x && neighbour.PositionCell.y == y && neighbour.PositionCell.z == z
                                && indexOfAtom == indexOfSelAtom) continue;
                            // Чтобы не брать исходный атом
                            if (startAtom.PositionCell.x == neighbour.PositionCell.x && startAtom.PositionCell.y == neighbour.PositionCell.y
                                && startAtom.PositionCell.z == neighbour.PositionCell.z && indexOfAtom == indexOfStartAtom) continue;

                            double radius = Vector.Magnitude(neighbour.GlobalCoord, selAtom.GlobalCoord);

                            if (radius <= radiusOfNeighbours)
                            {
                                // Второй сосед для исходного атома
                                neighbours.Add(neighbour);
                            }
                        }
                    }
        }

        /// <summary>
        /// Подсчёт полной потенциальной энергии системы
        /// </summary>
        /// <param name="tersoff">С потенциалом Терсоффа, либо Стиллинджера-Вебера</param>
        /// <returns></returns>
        public double SumEnergy(bool tersoff)
        {
            double sumEnergy = 0;
            int length = Structure.GetLength(0);


            if (tersoff)
            {
                for (int x = 0; x < length; x++)
                    for (int y = 0; y < length; y++)
                        for (int z = 0; z < length; z++)// Перебор ячеек структуры
                            for (int i = 0; i < Cell.NumberOfAtoms; i++)// Перебор атомов в ячейке
                            {
                                Atom selAtom = Structure[x, y, z].Atoms[i];
                                // Получаем первых и вторых соседей
                                Atom[] neighbours = SearchNeighbours(selAtom, i);

                                selAtom.Energy = potentialTersoff.PotentialEnergy(selAtom, neighbours);
                                sumEnergy += selAtom.Energy;

                                OutputInfo.InfoAtom(length, selAtom, i, neighbours);
                            }

                OutputInfo.FinishOutput();
            }
            else
            {
                for (int x = 0; x < length; x++)
                    for (int y = 0; y < length; y++)
                        for (int z = 0; z < length; z++)// Перебор ячеек структуры
                            for (int i = 0; i < Cell.NumberOfAtoms; i++)// Перебор атомов в ячейке
                            {
                                Atom selAtom = Structure[x, y, z].Atoms[i];
                                // Получаем первых и вторых соседей
                                Atom[] neighbours = SearchNeighbours(selAtom, i);

                                selAtom.Energy = potentialStillinger.PotentialEnergy(selAtom, neighbours);
                                sumEnergy += selAtom.Energy;

                                OutputInfo.InfoAtom(length, selAtom, i, neighbours);
                            }

                OutputInfo.FinishOutput();
            }
            return sumEnergy;
        }
        /// <summary>
        /// Формула трёхточечного дифференцирования для расчёта производной потенциальной энергии
        /// </summary>
        /// <param name="selAtom">Выбранный атом</param>
        /// <param name="indexOfSelAtom">Индекс выбранного атома в ячейке</param>
        /// <param name="delta">Смещение</param>
        /// <returns></returns>
        private double ThreePointDifferentiation(Atom selAtom, int indexOfSelAtom, Vector delta)
        {
            Atom[] neighbours;

            selAtom.LocalCoord = new Vector(selAtom.LocalCoord.x, selAtom.LocalCoord.y, selAtom.LocalCoord.z) + delta;// С учётом периодических граничных условий
            neighbours = SearchNeighbours(selAtom, indexOfSelAtom);
            double potentialEnergyRight = potentialTersoff.PotentialEnergy(selAtom, neighbours);

            selAtom.LocalCoord = new Vector(selAtom.LocalCoord.x, selAtom.LocalCoord.y, selAtom.LocalCoord.z) - (2 * delta);// С учётом периодических граничных условий
            neighbours = SearchNeighbours(selAtom, indexOfSelAtom);
            double potentialEnergyLeft = potentialTersoff.PotentialEnergy(selAtom, neighbours);

            return (potentialEnergyRight - potentialEnergyLeft) / (2.0 * delta.MaxElement());
        }
        /// <summary>
        /// Сила, действующая на выбранный атом
        /// </summary>
        /// <param name="selAtom">Выбранный атом</param>
        /// <param name="indexOfSelAtom">Индекс выбранного атома в ячейке</param>
        /// <returns></returns>
        private Vector Force(Atom selAtom, int indexOfSelAtom)
        {
            Vector force = new Vector();
            double valueDelta = Atom.LatPar * 0.01;// Сотая от параметра решётки?

            force.x = -ThreePointDifferentiation(selAtom, indexOfSelAtom, new Vector(valueDelta, 0.0, 0.0));
            force.y = -ThreePointDifferentiation(selAtom, indexOfSelAtom, new Vector(0.0, valueDelta, 0.0));
            force.z = -ThreePointDifferentiation(selAtom, indexOfSelAtom, new Vector(0.0, 0.0, valueDelta));

            return force;
        }

        // Алгоритм Верле
        private void AlgorithmVerlet(ref Atom selAtom, int indexOfSelAtom, double stepTime)
        {
            /*
             * Смещение координаты атома на delta происходит с учётом периодических граничных условий???
             */

            Vector speedup = Force(selAtom, indexOfSelAtom) / selAtom.Mass;// An
            Vector coordinate = selAtom.LocalCoord + selAtom.Velocity * stepTime + speedup * Math.Pow(stepTime, 2) / 2.0;

            selAtom.LocalCoord = coordinate;

            speedup += Force(selAtom, indexOfSelAtom) / selAtom.Mass;// An+1

            Vector velocity = selAtom.Velocity + speedup * stepTime / 2.0;

            selAtom.Velocity = velocity;
        }


    }
}