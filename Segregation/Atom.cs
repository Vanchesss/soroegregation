﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segregation
{
    /// <summary>
    /// Тип атома
    /// </summary>
    public enum AtomType
    {
        Si, // Silicium
        Ge // Germanium
    }
    public static class AtomMass
    {
        public static double MassOfSi = 28.068;// а.е.м.
        public static double MassOfGe = 72.630;// а.е.м.
    }
    public struct Atom
    {
        /// <summary>
        /// Параметр решётки
        /// </summary>
        public static double LatPar = 1.0;// Lattice parameter

        /// <summary>
        /// Тип атома
        /// </summary>
        public AtomType Type;
        // Локальные координаты атома внутри ячейки
        private Vector localCoord;
        /// <summary>
        /// Скорость атома
        /// </summary>
        public Vector Velocity;
        /// <summary>
        /// Позиция ячейки в которой находится атом
        /// </summary>
        public Vector PositionCell;
        /// <summary>
        /// Масса атома
        /// </summary>
        public double Mass;
        /// <summary>
        /// Энергия атома
        /// </summary>
        public double Energy;

        /// <summary>
        /// Координата атома с учётом ячейки, в которой он размещён
        /// </summary>
        public Vector GlobalCoord
        {
            get { return localCoord + (PositionCell * LatPar); }
        }
        /// <summary>
        /// Координаты атома с учётом периодических граничных условий
        /// </summary>
        public Vector LocalCoord
        {
            set
            {
                if (value.x > LatPar) localCoord.x = value.x % LatPar;
                else if (value.x < 0.0) localCoord.x = LatPar + (value.x % LatPar);
                else localCoord.x = value.x;

                if (value.y > LatPar) localCoord.y = value.y % LatPar;
                else if (value.y < 0.0) localCoord.y = LatPar + (value.y % LatPar);
                else localCoord.y = value.y;

                if (value.z > LatPar) localCoord.z = value.z % LatPar;
                else if (value.z < 0.0) localCoord.z = LatPar + (value.z % LatPar);
                else localCoord.z = value.z;
            }

            get
            {
                return localCoord;
            }
        }
    }
}