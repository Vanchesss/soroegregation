﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segregation
{
    // Абстрактный класс для потенциалов - для каждого можно вызвать одинаковый метод
    public abstract class Potential
    {
        public abstract double PotentialEnergy(Atom selAtom, Atom[] neighbours);
    }

    public class PotentialTersoff : Potential
    {
        // Параметры потенциала Терсоффа
        private struct ParamPotential
        {
            public double A, B, S, R, b, c, d, n, h, l1, l2, l3;

            public ParamPotential(double A, double B, double S, double R, double b, double c, double d, double n, double h, double l1, double l2, double l3)
            {
                this.A = A;
                this.B = B;
                this.S = S;
                this.R = R;
                this.b = b;
                this.c = c;
                this.d = d;
                this.n = n;
                this.h = h;
                this.l1 = l1;
                this.l2 = l2;
                this.l3 = l3;
            }
            // Используется для получения параметров SiGe
            public ParamPotential(ParamPotential potential1, ParamPotential potential2)
            {
                this.A = 0.5 * (potential1.A + potential2.A);
                this.B = 0.5 * (potential1.B + potential2.B);
                this.S = 0.5 * (potential1.S + potential2.S);
                this.R = 0.5 * (potential1.R + potential2.R);
                this.b = 0.5 * (potential1.b + potential2.b);
                this.c = 0.5 * (potential1.c + potential2.c);
                this.d = 0.5 * (potential1.d + potential2.d);
                this.n = 0.5 * (potential1.n + potential2.n);
                this.h = 0.5 * (potential1.h + potential2.h);
                this.l1 = 0.5 * (potential1.l1 + potential2.l1);
                this.l2 = 0.5 * (potential1.l2 + potential2.l2);
                this.l3 = 0.5 * (potential1.l3 + potential2.l3);
            }
        }
        // Наборы параметров для различных соединений атомов
        private ParamPotential paramOfSi, paramOfGe, paramOfSiGe;

        public PotentialTersoff()
        {
            paramOfSi = new ParamPotential(1830.8, 471.18, 0.30, 0.27, 1.1e-6, 100390, 16.217, 0.78734, -0.59825, 24.799, 17.322, 0);
            paramOfGe = new ParamPotential(1769, 419.23, 0.31, 0.28, 9.01e-7, 106430, 15.65, 0.75627, -0.43884, 24.451, 17.047, 0);
            paramOfSiGe = new ParamPotential(paramOfSi, paramOfGe);
        }

        /*
         * Вспомогательные функции для расчёта потенциала
         */

        /// <summary>
        /// Функция обрезания Fc
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="radius">Расстояние до атома-соседа</param>
        /// <returns></returns>
        private double PE_FunctionCutoff(ParamPotential potential, double radius)
        {
            if (radius < potential.R) return 1.0;
            else if (radius > potential.S) return 0.0;
            else return 0.5 * (1.0 + Math.Cos(Math.PI * (radius - potential.R) / (2.0 * (potential.S - potential.R))));
        }
        /// <summary>
        /// Функция притяжения Fa(r)
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="Rij">Расстояние до атома-соседа</param>
        /// <returns></returns>
        private double PE_FunctionA(ParamPotential potential, double Rij)
        {
            return potential.A * Math.Exp(-potential.l1 * Rij);
        }
        /// <summary>
        /// Функция отталкивания Fr(r)
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="Rij">Расстояние до атома-соседа</param>
        /// <returns></returns>
        private double PE_FunctionB(ParamPotential potential, double Rij)
        {
            return -potential.B * Math.Exp(-potential.l2 * Rij);
        }
        /// <summary>
        /// Функция G(O)
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="Rij">Расстояние до первого атома-соседа</param>
        /// <param name="Rik">Расстояние до второго атома-соседа</param>
        /// <param name="Rjk">Расстояние между первым атомом-соседом и вторым атомом-соседом</param>
        /// <returns></returns>
        private double PE_FunctionG(ParamPotential potential, double Rij, double Rik, double Rjk)
        {
            // Косинус угла между связями i<->j и i<->k вычисляется по теореме косинусов:
            double cosTeta = (Math.Pow(Rik, 2) + Math.Pow(Rij, 2) - Math.Pow(Rjk, 2)) / (2.0 * Rij * Rik);

            // Возвели параметры в квадрат отдельно чтобы не писать ниже огромной формулой:
            double c2 = Math.Pow(potential.c, 2);//с^2
            double d2 = Math.Pow(potential.d, 2);//d^2

            return 1.0 + (c2 / d2) - (c2 / (d2 + Math.Pow(potential.h - cosTeta, 2)));
        }
        /// <summary>
        /// Функция Дзета_ij
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="selAtom">Выбранный атом (i)</param>
        /// <param name="atomNeighbours">Атомы-соседи</param>
        /// <param name="indexNeighbour">Индекс в массиве соседей первого атома-соседа (j)</param>
        /// <returns></returns>
        private double PE_FunctionDzeta(ParamPotential potential, Atom selAtom, Atom[] atomNeighbours, int indexNeighbour)
        {
            double sum = 0.0;
            double Rij = Vector.Magnitude(selAtom.GlobalCoord, atomNeighbours[indexNeighbour].GlobalCoord);

            for (int k = 0; k < atomNeighbours.Length; k++)
            {
                if (k == indexNeighbour) continue;
                double Rik = Vector.Magnitude(selAtom.GlobalCoord, atomNeighbours[k].GlobalCoord);
                double Rjk = Vector.Magnitude(atomNeighbours[indexNeighbour].GlobalCoord, atomNeighbours[k].GlobalCoord);

                double exponenta = potential.l3 == 0 ? 1 : Math.Exp(Math.Pow(potential.l3, 3) * Math.Pow(Rij - Rik, 3));
                sum += PE_FunctionCutoff(potential, Rik) * PE_FunctionG(potential, Rij, Rik, Rjk) * exponenta;
            }

            return sum;
        }
        /// <summary>
        /// Функция bij
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="selAtom">Выбранный атом (i)</param>
        /// <param name="atomNeighbours">Атомы-соседи</param>
        /// <param name="indexNeighbour">Индекс в массиве соседей первого атома-соседа (j)</param>
        /// <returns></returns>
        private double PE_FunctionBeta(ParamPotential potential, Atom selAtom, Atom[] atomNeighbours, int indexNeighbour)
        {
            double dzeta = PE_FunctionDzeta(potential, selAtom, atomNeighbours, indexNeighbour);

            return Math.Pow(1.0 + Math.Pow(potential.b * dzeta, potential.n), -1.0 / (2.0 * potential.n));
        }



        /// <summary>
        /// Потенциальная энергия выбранного атома
        /// </summary>
        /// <param name="selAtom">Выбранный атом</param>
        /// <param name="indexSelAtom">Индекс выбранного атома в ячейке</param>
        /// <param name="atomNeighbours">Атомы-соседи</param>
        /// <returns></returns>
        public override double PotentialEnergy(Atom selAtom, Atom[] neighbours)
        {
            double atomEnergy = 0.0;

            for (int j = 0; j < neighbours.Length; j++)
            {
                double Rij = Vector.Magnitude(selAtom.GlobalCoord, neighbours[j].GlobalCoord);

                // В зависимости от типов атомов меняются параметры потенциала
                ParamPotential potential = paramOfSiGe;
                if (selAtom.Type == AtomType.Si && selAtom.Type == neighbours[j].Type) potential = paramOfSi;
                if (selAtom.Type == AtomType.Ge && selAtom.Type == neighbours[j].Type) potential = paramOfGe;

                double beta = PE_FunctionBeta(potential, selAtom, neighbours, j);
                double Vij = PE_FunctionCutoff(potential, Rij) * (PE_FunctionA(potential, Rij) + beta * PE_FunctionB(potential, Rij));

                atomEnergy += Vij;
            }

            atomEnergy /= 2.0;
            return atomEnergy;
        }
    }

    public class PotentialStillinger : Potential
    {
        // Параметры потенциала Стиллинджера-Вебера
        private struct ParamPotential
        {
            public double A, B, p, a, l, y;
            public ParamPotential(double A, double B, double p, double a, double l, double y)
            {
                this.A = A;
                this.B = B;
                this.p = p;
                this.a = a;
                this.l = l;
                this.y = y;
            }
            /*
            public ParamPotential(ParamPotential potential1, ParamPotential potential2)
            {
                this.A = Math.Sqrt(potential1.A * potential2.A);
                this.B = Math.Sqrt(potential1.B * potential2.B);
                this.p = Math.Sqrt(potential1.p * potential2.p);
                this.a = Math.Sqrt(potential1.a * potential2.a);
                this.l = 0.5 * (potential1.l + potential2.l);
                this.y = 0.5 * (potential1.y + potential2.y);
            }*/
        }
        // Наборы параметров для различных соединений атомов
        private ParamPotential paramOfSi, paramOfGe, paramOfSiGe;

        public PotentialStillinger()
        {
            paramOfSi = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80, 21.0, 1.20);
            paramOfGe = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80, 31.0, 1.20);
            paramOfSiGe = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80, 24.333, 1.20);
        }

        /*
         * Вспомогательные функции для расчёта потенциала
         */

        /// <summary>
        /// Функция двухчастичного взаимодействия ф(r)
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="radius">Расстояние до атома-соседа</param>
        /// <returns></returns>
        private double PE_FunctionCutoff(ParamPotential potential, double radius)
        {
            if (radius >= potential.a) return 0.0;
            else return potential.A * (potential.B * Math.Pow(radius, -potential.p) - 1.0) * Math.Exp(Math.Pow(radius - potential.a, -1.0));
        }
        /// <summary>
        /// Затухающая функция, критический радиус которой находится между первым и вторым ближайшим соседом
        /// </summary>
        /// <param name="potential">Параметры потенциала</param>
        /// <param name="Rij">Расстояние до первого атома-соседа</param>
        /// <param name="Rik">Расстояние до второго атома-соседа</param>
        /// <returns></returns>
        private double PE_FunctionH(ParamPotential potential, double Rij, double Rik)
        {
            if (Rij >= potential.a) return 0.0;
            else if (Rik >= potential.a) return 0.0;
            else
            {
                double exponenta = (potential.y / (Rij - potential.a)) + (potential.y / (Rik - potential.a));
                return potential.l * Math.Exp(exponenta);
            }
        }
        /// <summary>
        /// Функция (cos + 1/3)^2
        /// </summary>
        /// <param name="Rij">Расстояние до первого атома-соседа</param>
        /// <param name="Rik">Расстояние до второго атома-соседа</param>
        /// <param name="Rjk">Расстояние между первым атомом-соседом и вторым атомом-соседом</param>
        /// <returns></returns>
        private double PE_FunctionCos(double Rij, double Rik, double Rjk)
        {
            // Косинус угла между связями i<->j и i<->k вычисляется по теореме косинусов:
            double cosTeta = (Math.Pow(Rik, 2) + Math.Pow(Rij, 2) - Math.Pow(Rjk, 2)) / (2.0 * Rij * Rik);
            return Math.Pow(cosTeta + (1.0 / 3.0), 2.0);
        }



        /// <summary>
        /// Потенциальная энергия выбранного атома
        /// </summary>
        /// <param name="selAtom">Выбранный атом</param>
        /// <param name="indexSelAtom">Индекс выбранного атома в ячейке</param>
        /// <param name="atomNeighbours">Атомы-соседи</param>
        /// <returns></returns>
        public override double PotentialEnergy(Atom selAtom, Atom[] neighbours)
        {
            double atomEnergy = 0.0;

            for (int j = 0; j < neighbours.Length; j++)
            {
                double Rij = Vector.Magnitude(selAtom.GlobalCoord, neighbours[j].GlobalCoord);

                // В зависимости от типов атомов меняются параметры потенциала
                ParamPotential potential = paramOfSiGe;
                if (selAtom.Type == AtomType.Si && selAtom.Type == neighbours[j].Type) potential = paramOfSi;
                if (selAtom.Type == AtomType.Ge && selAtom.Type == neighbours[j].Type) potential = paramOfGe;

                atomEnergy += 0.5 * PE_FunctionCutoff(potential, Rij);

                for (int k = 0; k < neighbours.Length; k++)
                {
                    if (k == j) continue;
                    double Rik = Vector.Magnitude(selAtom.GlobalCoord, neighbours[k].GlobalCoord);
                    double Rjk = Vector.Magnitude(neighbours[j].GlobalCoord, neighbours[k].GlobalCoord);

                    atomEnergy += (PE_FunctionH(potential, Rij, Rik) + PE_FunctionH(potential, Rij, Rjk) +
                        PE_FunctionH(potential, Rik, Rjk)) * PE_FunctionCos(Rij, Rik, Rjk);
                }
            }

            return atomEnergy;
        }
    }
}