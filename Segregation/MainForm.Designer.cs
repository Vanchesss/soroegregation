﻿namespace Segregation
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.TabControl tabControl1;
            System.Windows.Forms.PictureBox pictureBox1;
            System.Windows.Forms.PictureBox pictureBox2;
            System.Windows.Forms.GroupBox groupBox1;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.radioButton_STW = new System.Windows.Forms.RadioButton();
            this.radioButton_TRS = new System.Windows.Forms.RadioButton();
            this.button_Calculated = new System.Windows.Forms.Button();
            this.textBox_SumEnergy = new System.Windows.Forms.TextBox();
            this.numUD_Size = new System.Windows.Forms.NumericUpDown();
            this.button_Close = new System.Windows.Forms.Button();
            this.numUD_N = new System.Windows.Forms.NumericUpDown();
            this.button_OpenFile = new System.Windows.Forms.Button();
            this.numUD_LatPar = new System.Windows.Forms.NumericUpDown();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            tabControl1 = new System.Windows.Forms.TabControl();
            pictureBox1 = new System.Windows.Forms.PictureBox();
            pictureBox2 = new System.Windows.Forms.PictureBox();
            groupBox1 = new System.Windows.Forms.GroupBox();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(pictureBox2)).BeginInit();
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_Size)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_N)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_LatPar)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(12, 93);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(97, 13);
            label1.TabIndex = 2;
            label1.Text = "Энергия системы";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(12, 14);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(94, 13);
            label2.TabIndex = 4;
            label2.Text = "Размер системы";
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(this.tabPage1);
            tabControl1.Controls.Add(this.tabPage2);
            tabControl1.Location = new System.Drawing.Point(12, 142);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new System.Drawing.Size(640, 339);
            tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(pictureBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(632, 313);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Потенциал Терсоффа";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            pictureBox1.BackColor = System.Drawing.Color.White;
            pictureBox1.Image = global::Segregation.Properties.Resources.ImagePotentialOfTersoff;
            pictureBox1.Location = new System.Drawing.Point(6, 6);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new System.Drawing.Size(620, 300);
            pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            pictureBox1.TabIndex = 6;
            pictureBox1.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(pictureBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(632, 313);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Потенциал Стиллинджера-Вебера";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            pictureBox2.BackColor = System.Drawing.Color.White;
            pictureBox2.Image = global::Segregation.Properties.Resources.ImagePotentialOfStillinger;
            pictureBox2.Location = new System.Drawing.Point(6, 6);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new System.Drawing.Size(620, 300);
            pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            pictureBox2.TabIndex = 7;
            pictureBox2.TabStop = false;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(this.radioButton_STW);
            groupBox1.Controls.Add(this.radioButton_TRS);
            groupBox1.Location = new System.Drawing.Point(196, 12);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new System.Drawing.Size(177, 52);
            groupBox1.TabIndex = 13;
            groupBox1.TabStop = false;
            groupBox1.Text = "Тип потенциала";
            // 
            // radioButton_STW
            // 
            this.radioButton_STW.AutoSize = true;
            this.radioButton_STW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_STW.Location = new System.Drawing.Point(73, 21);
            this.radioButton_STW.Name = "radioButton_STW";
            this.radioButton_STW.Size = new System.Drawing.Size(95, 17);
            this.radioButton_STW.TabIndex = 9;
            this.radioButton_STW.Text = "StillingerWeber";
            this.radioButton_STW.UseVisualStyleBackColor = true;
            // 
            // radioButton_TRS
            // 
            this.radioButton_TRS.AutoSize = true;
            this.radioButton_TRS.Checked = true;
            this.radioButton_TRS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton_TRS.Location = new System.Drawing.Point(9, 21);
            this.radioButton_TRS.Name = "radioButton_TRS";
            this.radioButton_TRS.Size = new System.Drawing.Size(57, 17);
            this.radioButton_TRS.TabIndex = 8;
            this.radioButton_TRS.TabStop = true;
            this.radioButton_TRS.Text = "Tersoff";
            this.radioButton_TRS.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(12, 40);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(51, 13);
            label3.TabIndex = 12;
            label3.Text = "Доля Ge";
            // 
            // button_Calculated
            // 
            this.button_Calculated.BackColor = System.Drawing.Color.White;
            this.button_Calculated.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Calculated.Location = new System.Drawing.Point(115, 88);
            this.button_Calculated.Name = "button_Calculated";
            this.button_Calculated.Size = new System.Drawing.Size(75, 23);
            this.button_Calculated.TabIndex = 0;
            this.button_Calculated.Text = "Подсчёт";
            this.button_Calculated.UseVisualStyleBackColor = false;
            this.button_Calculated.Click += new System.EventHandler(this.button_Calculated_Click);
            // 
            // textBox_SumEnergy
            // 
            this.textBox_SumEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_SumEnergy.Location = new System.Drawing.Point(196, 91);
            this.textBox_SumEnergy.Name = "textBox_SumEnergy";
            this.textBox_SumEnergy.Size = new System.Drawing.Size(177, 20);
            this.textBox_SumEnergy.TabIndex = 1;
            // 
            // numUD_Size
            // 
            this.numUD_Size.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_Size.Location = new System.Drawing.Point(112, 12);
            this.numUD_Size.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numUD_Size.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numUD_Size.Name = "numUD_Size";
            this.numUD_Size.Size = new System.Drawing.Size(42, 20);
            this.numUD_Size.TabIndex = 3;
            this.numUD_Size.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // button_Close
            // 
            this.button_Close.BackColor = System.Drawing.Color.White;
            this.button_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Close.Location = new System.Drawing.Point(577, 88);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(75, 23);
            this.button_Close.TabIndex = 10;
            this.button_Close.Text = "Выход";
            this.button_Close.UseVisualStyleBackColor = false;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // numUD_N
            // 
            this.numUD_N.DecimalPlaces = 2;
            this.numUD_N.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numUD_N.Location = new System.Drawing.Point(69, 38);
            this.numUD_N.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_N.Name = "numUD_N";
            this.numUD_N.Size = new System.Drawing.Size(43, 20);
            this.numUD_N.TabIndex = 11;
            // 
            // button_OpenFile
            // 
            this.button_OpenFile.BackColor = System.Drawing.Color.White;
            this.button_OpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_OpenFile.Location = new System.Drawing.Point(470, 40);
            this.button_OpenFile.Name = "button_OpenFile";
            this.button_OpenFile.Size = new System.Drawing.Size(66, 23);
            this.button_OpenFile.TabIndex = 14;
            this.button_OpenFile.Text = "Открыть";
            this.button_OpenFile.UseVisualStyleBackColor = false;
            this.button_OpenFile.Click += new System.EventHandler(this.button_OpenFile_Click);
            // 
            // numUD_LatPar
            // 
            this.numUD_LatPar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_LatPar.DecimalPlaces = 2;
            this.numUD_LatPar.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numUD_LatPar.Location = new System.Drawing.Point(494, 12);
            this.numUD_LatPar.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_LatPar.Name = "numUD_LatPar";
            this.numUD_LatPar.Size = new System.Drawing.Size(42, 20);
            this.numUD_LatPar.TabIndex = 15;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(384, 14);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(104, 13);
            label4.TabIndex = 16;
            label4.Text = "Параметр решётки";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(384, 45);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(80, 13);
            label5.TabIndex = 17;
            label5.Text = "Файл отладки";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 142);
            this.Controls.Add(label5);
            this.Controls.Add(label4);
            this.Controls.Add(this.numUD_LatPar);
            this.Controls.Add(this.button_OpenFile);
            this.Controls.Add(groupBox1);
            this.Controls.Add(label3);
            this.Controls.Add(this.numUD_N);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(tabControl1);
            this.Controls.Add(label2);
            this.Controls.Add(this.numUD_Size);
            this.Controls.Add(label1);
            this.Controls.Add(this.textBox_SumEnergy);
            this.Controls.Add(this.button_Calculated);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(680, 530);
            this.MinimumSize = new System.Drawing.Size(680, 180);
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Сегрегация SiGe";
            tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(pictureBox2)).EndInit();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_Size)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_N)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_LatPar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Calculated;
        private System.Windows.Forms.TextBox textBox_SumEnergy;
        private System.Windows.Forms.NumericUpDown numUD_Size;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RadioButton radioButton_TRS;
        private System.Windows.Forms.RadioButton radioButton_STW;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.NumericUpDown numUD_N;
        private System.Windows.Forms.Button button_OpenFile;
        private System.Windows.Forms.NumericUpDown numUD_LatPar;
    }
}

