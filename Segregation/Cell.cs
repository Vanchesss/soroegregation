﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segregation
{
    public class Cell
    {
        /// <summary>
        /// Количество атомов в ячейке
        /// </summary>
        public const int NumberOfAtoms = 8;

        /// <summary>
        /// Атомы в ячейке
        /// </summary>
        public Atom[] Atoms = new Atom[NumberOfAtoms];

        /// <summary>
        /// Создание ячейки
        /// </summary>
        /// <param name="x">Позиция ячейки по X</param>
        /// <param name="y">Позиция ячейки по Y</param>
        /// <param name="z">Позиция ячейки по Z</param>
        public Cell(int x, int y, int z)
        {
            double koff = 0.25d;
            koff *= Atom.LatPar;

            Atoms[0].LocalCoord = new Vector(0, 0, 0) * koff;
            Atoms[1].LocalCoord = new Vector(0, 2, 2) * koff;
            Atoms[2].LocalCoord = new Vector(2, 0, 2) * koff;
            Atoms[3].LocalCoord = new Vector(2, 2, 0) * koff;

            Atoms[4].LocalCoord = new Vector(1, 1, 1) * koff;
            Atoms[5].LocalCoord = new Vector(1, 3, 3) * koff;
            Atoms[6].LocalCoord = new Vector(3, 1, 3) * koff;
            Atoms[7].LocalCoord = new Vector(3, 3, 1) * koff;

            // Задаём атомам позицию ячейки, в которой они находятся
            for (int i = 0; i < Cell.NumberOfAtoms; i++)
            {
                Atoms[i].PositionCell = new Vector(x, y, z);
            }
        }

        /// <summary>
        /// Создание ячейки с определённым типом атомов
        /// </summary>
        /// <param name="x">Позиция ячейки по X</param>
        /// <param name="y">Позиция ячейки по Y</param>
        /// <param name="z">Позиция ячейки по Z</param>
        /// <param name="type">Тип атомов в ячейке</param>
        public Cell(int x, int y, int z, AtomType type)
        {
            double koff = 0.25d;
            koff *= Atom.LatPar;

            Atoms[0].LocalCoord = new Vector(0, 0, 0) * koff;
            Atoms[1].LocalCoord = new Vector(0, 2, 2) * koff;
            Atoms[2].LocalCoord = new Vector(2, 0, 2) * koff;
            Atoms[3].LocalCoord = new Vector(2, 2, 0) * koff;

            Atoms[4].LocalCoord = new Vector(1, 1, 1) * koff;
            Atoms[5].LocalCoord = new Vector(1, 3, 3) * koff;
            Atoms[6].LocalCoord = new Vector(3, 1, 3) * koff;
            Atoms[7].LocalCoord = new Vector(3, 3, 1) * koff;

            // Задаём атомам тип, массу и позицию ячейки, в которой они находятся
            for (int i = 0; i < Cell.NumberOfAtoms; i++)
            {
                Atoms[i].Type = type;
                Atoms[i].Mass = (type == AtomType.Si ? AtomMass.MassOfSi : AtomMass.MassOfGe);
                Atoms[i].PositionCell = new Vector(x, y, z);
            }
        }
    }
}