﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Segregation
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button_Calculated_Click(object sender, EventArgs e)
        {
            Segregation sige = new Segregation((int)numUD_Size.Value, (double)numUD_N.Value, (double)numUD_LatPar.Value);

            double energy = sige.SumEnergy(radioButton_TRS.Checked);
            textBox_SumEnergy.Text = energy.ToString();
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_OpenFile_Click(object sender, EventArgs e)
        {
            OutputInfo.OpenNotepad();
        }
    }
}