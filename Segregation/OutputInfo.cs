﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace Segregation
{
    public static class OutputInfo
    {
        private static string path = Environment.CurrentDirectory + "\\DebuggingNotepad.txt";
        private static bool Start = false;

        public static void FinishOutput()
        {
            Start = false;
        }

        public static void InfoAtom(int length, Atom selAtom, int indexOfSelAtom, Atom[] neighbours)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(path, Start, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[Информация об атоме]");

                    sw.Write("Позиция ячейки в которой находится атом: (");
                    sw.Write(selAtom.PositionCell.x.ToString() + "; ");
                    sw.Write(selAtom.PositionCell.y.ToString() + "; ");
                    sw.Write(selAtom.PositionCell.z.ToString() + ")" + Environment.NewLine);
                    /*Vector posCell = selAtom.PositionCell;
                    if (posCell.x < 0) posCell.x += length;
                    if (posCell.x >= length) posCell.x -= length;
                    if (posCell.y < 0) posCell.y += length;
                    if (posCell.y >= length) posCell.y -= length;
                    if (posCell.z < 0) posCell.z += length;
                    if (posCell.z >= length) posCell.z -= length;

                    sw.Write(posCell.x.ToString() + "; ");
                    sw.Write(posCell.y.ToString() + "; ");
                    sw.Write(posCell.z.ToString() + ")" + Environment.NewLine);*/

                    sw.Write("Индекс атома в ячейке: ");
                    sw.Write(indexOfSelAtom.ToString() + Environment.NewLine);

                    sw.Write("Координаты атома: (");
                    sw.Write(selAtom.LocalCoord.x.ToString() + "; ");
                    sw.Write(selAtom.LocalCoord.y.ToString() + "; ");
                    sw.Write(selAtom.LocalCoord.z.ToString() + ")" + Environment.NewLine);

                    sw.Write("Глобальные координаты атома: (");
                    sw.Write(selAtom.GlobalCoord.x.ToString() + "; ");
                    sw.Write(selAtom.GlobalCoord.y.ToString() + "; ");
                    sw.Write(selAtom.GlobalCoord.z.ToString() + ")" + Environment.NewLine);

                    sw.Write("Энергия атома: ");
                    sw.Write(selAtom.Energy.ToString() + Environment.NewLine);

                    sw.Write(Environment.NewLine);

                    sw.WriteLine("<<<ПЕРВЫЕ И ВТОРЫЕ СОСЕДИ АТОМА>>>");
                    for (int i = 0; i < neighbours.Length; i++)
                    {
                        sw.WriteLine(String.Format("Сосед №{0}:", i + 1));

                        Atom atom = neighbours[i];

                        /*posCell = atom.PositionCell;
                        if (posCell.x < 0) posCell.x += length;
                        if (posCell.x >= length) posCell.x -= length;
                        if (posCell.y < 0) posCell.y += length;
                        if (posCell.y >= length) posCell.y -= length;
                        if (posCell.z < 0) posCell.z += length;
                        if (posCell.z >= length) posCell.z -= length;*/

                        sw.Write("\tПозиция ячейки в которой находится атом: (");
                        sw.Write(atom.PositionCell.x.ToString() + "; ");
                        sw.Write(atom.PositionCell.y.ToString() + "; ");
                        sw.Write(atom.PositionCell.z.ToString() + ")" + Environment.NewLine);

                        sw.Write("\tКоординаты атома: (");
                        sw.Write(atom.LocalCoord.x.ToString() + "; ");
                        sw.Write(atom.LocalCoord.y.ToString() + "; ");
                        sw.Write(atom.LocalCoord.z.ToString() + ")" + Environment.NewLine);

                        sw.Write("\tГлобальные координаты атома: (");
                        sw.Write(atom.GlobalCoord.x.ToString() + "; ");
                        sw.Write(atom.GlobalCoord.y.ToString() + "; ");
                        sw.Write(atom.GlobalCoord.z.ToString() + ")" + Environment.NewLine);

                        sw.Write(Environment.NewLine);
                    }

                    Start = true;
                }
            }
            catch
            {
                Console.WriteLine("Ошибка вывода в файл");
            }
        }
        public static void OpenNotepad()
        {
            Process.Start(path);
        }
    }
}
